#!/usr/bin/python

""" Sends a message to a listening server"""

__author__ = 'Encryptio'

import sys
import socket


def send(hostname, port, message):
    """
    Sends a message to the specified hostname on the specified port

    :param hostname:
    :param port:
    :param message:
    :return:
    """

    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    try:
        s.sendto(message.encode('utf-8'), (hostname, port))
        print('[+] Message sent')
    except Exception as e:
        sys.exit('[-] ' + str(e))


def main():
    send('', 12000, 'Hello') # replace '' with the hostname, 12000 with the port, 'Hello' with
                                           # the message


if __name__ == '__main__':
    main()
