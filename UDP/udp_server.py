#!/usr/bin/python3

"""A simple UDP server socket"""

__author__ = 'Encryptio'

import sys
import socket


def listen(port):
    """
    Listens on the specified port and accepts incoming connections

    :param port:
    :return:
    """

    server_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    # explicitly assigning the port number to the socket
    # whenever someone sends a packet to port 1200, the packet will be directed to the socket
    server_socket.bind(('', port))  # binds (assigns) the port to the server's socket
    print('[*] The server socket is ready to receive')

    while 1:    # wait for a packet to arrive
        try:
            message, addr = server_socket.recvfrom(1500)
            print('[+] From ' + str(addr) + ': ' + message.decode('utf-8'))
        except Exception as e:
            sys.exit('[-] ' + str(e))


def main():
    listen(12000) # replace with port to listen on


if __name__ == '__main__':
    main()
