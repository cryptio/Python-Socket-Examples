#!/usr/bin/python3

"""Establishes a TCP connection over SSL with a listening server and sends a message"""

__author__ = 'Encryptio'

import sys
import socket
import ssl


def connect(hostname, port, message):
    """
    Connects to the hostname on the specified port and sends a message

    :param hostname:
    :param port:
    :param message:
    :return:
    """

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # create a TCP socket

    try:
        s.connect((hostname, port))  # establish a TCP connection to the host
        # automatically loads the system's trusted CA certs and uses secure settings
        # parameter specifies that the context will be used to authenticate server-side sockets
        context = ssl.create_default_context(purpose=ssl.Purpose.SERVER_AUTH)
        # wraps an existing socket tied to its context
        sslsock = context.wrap_socket(s, server_hostname=hostname)

        # Check if the server really matches the hostname to which we are trying to connect
        ssl.match_hostname(sslsock.getpeercert(), hostname)

        sslsock.send(message.encode('utf-8'))  # send the message through the SSL socket
    except Exception as e:
        sys.exit('[-] ' + str(e))
        
    sslsock.shutdown(socket.SHUT_RDWR)
    sslsock.close()


def main():
    # replace '' with the hostname, 12000 as the port, and 'Client says hello' as the message
    connect('', 12000, 'Client says hello')


if __name__ == '__main__':
    main()

