#!/usr/bin/python3

"""A simple TCP server socket over SSL"""

__author__ = 'Encryptio'

import sys
import socket
import ssl

def listen(port):
    """
    Listens on the specified port and accepts incoming connections

    :param port:
    :return:
    """

    # server_socket - welcoming socket
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.bind(('', port))  # establish welcoming socket

    ca_certs_path = ''  # the public key that the client gets
    priv_certs_path = ''  # the private key

    # listen for TCP connection requests.
    server_socket.listen(1)  # parameter specifies the maximum number of queued connections (at least 1)

    # automatically use secure settings
    # parameter specifies that the context will be used to authenticate client-side sockets
    ctx = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH)
    ctx.load_cert_chain(certfile=ca_certs_path, keyfile=priv_certs_path)

    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    print('[*] The server is ready to receive')

    while 1:
        try:
            # when a client knocks on this door, the program calls the accept() method for
            # server_socket, which creates a new socket in the server, called connection_socket,
            # dedicated to this particular client. The client and server then complete
            # the handshaking, creating a TCP connection between the client's client_socket
            # and the server's connection_socket
            connection_socket, addr = server_socket.accept()
            # wraps an existing socket tied to its context
            sslsock = ctx.wrap_socket(connection_socket, server_side=True)
            data = sslsock.read()  # read data from the SSL socket

            hostname, port = addr  # unpack the tuple
            print('[+] From ' + hostname + ' on port ' + str(port) + ': ' +
                  data.decode('utf-8'))
        except Exception as e:
            sys.exit('[-] ' + str(e))

        sslsock.shutdown(socket.SHUT_RDWR)
        sslsock.close()


def main():
    listen(12000)


if __name__ == '__main__':
    main()
