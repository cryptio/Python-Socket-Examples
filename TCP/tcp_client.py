#!/usr/bin/python3

"""Establishes a TCP connection with a listening server and sends a message"""

__author__ = 'Encryptio'

import sys
import socket
import os


def connect(hostname, port, message):
    """
    Connects to the hostname on the specified port and sends a message

    :param hostname:
    :param port:
    :param message:
    :return:
    """

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # create a TCP socket
    try:
        s.connect((hostname, port))  # establish a TCP connection to the host
        s.send(message.encode('utf-8'))  # send the message
    except Exception as e:
        sys.exit('[-]' + str(e))


def main():
    connect('', 12000, 'Hello')  # replace '' with the hostname, 12000 as the port, and 'Hello' as the message


if __name__ == '__main__':
    main()
