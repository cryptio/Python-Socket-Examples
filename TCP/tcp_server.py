#!/usr/bin/python3

"""A simple TCP server socket"""

__author__ = 'Encryptio'


import sys
import socket


def listen(port):
    """
    Listens on the specified port and accepts incoming connections

    :param port:
    :return:
    """

    # server_socket - welcoming socket
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.bind(('', port))  # establish welcoming socket

    # listen for TCP connection requests.
    server_socket.listen(1)  # parameter specifies the maximum number of queued connections (at least 1)

    print('[*] The server is ready to receive')

    while 1:
        try:
            # When a client knocks on this door, the program invokes the accept() method for
            # server_socket, which creates a new socket in the server, called connec-
            # tion_ocket, dedicated to this particular client. The client and server then complete
            # the handshaking, creating a TCP connection between the client’s client_socket
            # and the server’s connection_socket
            connection_socket, addr = server_socket.accept()
            message = connection_socket.recv(1500)

            print('[+] From' + str(addr) + ': ' + message.decode('utf-8'))
        except Exception as e:
            sys.exit('[-] ' +  str(e))

        connection_socket.close()


def main():
    listen(12000)  # replace with the port to listen on


if __name__ == '__main__':
    main()
